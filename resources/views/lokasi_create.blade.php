@extends('base')
@section('content')
    <!-- Main Section -->
    <section class="main-section">
        <!-- Add Your Content Inside -->
        <div class="content">
            <!-- Remove This Before You Start -->

            <hr>
            <form action="{{ route('lokasi.store') }}" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="kode">Kode Lokasi:</label>
                    <input type="text" class="form-control" id="usr" name="kode">
                </div>
                <div class="form-group">
                    <label for="nama">Nama Lokasi:</label>
                    <input type="text" class="form-control" id="nama" name="nama">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-md btn-primary">Submit</button>
                    <button type="reset" class="btn btn-md btn-danger">Cancel</button>
                </div>
            </form>
        </div>
        <!-- /.content -->
    </section>
    <!-- /.main-section -->
@endsection