@extends('base')
@section('content')
    <!-- Main Section -->
    <section class="main-section">
        <!-- Add Your Content Inside -->
        <div class="content">
            <!-- Remove This Before You Start -->

            <hr>
            <form action="{{ route('pengiriman.store') }}" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="no_pengiriman">No Pengiriman:</label>
                    <input type="text" class="form-control" id="usr" name="no_pengiriman">
                </div>
                <div class="form-group">
                    <label for="tanggal">Tanggal:</label>
                    <input type="date" class="form-control" id="tanggal" name="tanggal">
                </div>
                <div class="form-group">
                    <label for="lokasi_id">Lokasi ID:</label>
                    <input type="number" class="form-control" id="lokasi_id" name="lokasi_id">
                </div>
                <div class="form-group">
                    <label for="barang_id">Barang ID:</label>
                    <input type="number" class="form-control" id="barang_id" name="barang_id">
                </div>
                <div class="form-group">
                    <label for="jumlah_barang">Jumlah Barang:</label>
                    <input type="number" class="form-control" id="jumlah_barang" name="jumlah_barang">
                </div>
                <div class="form-group">
                    <label for="harga_barang">Harga Barang:</label>
                    <input type="text" class="form-control" id="harga_barang" name="harga_barang">
                </div>
                <div class="form-group">
                    <label for="kurir_id">Kurir ID:</label>
                    <input type="number" class="form-control" id="kurir_id" name="kurir_id">
                </div>
                <div class="form-group">
                    <label for="status">Status Pengiriman:</label>
                    <input type="text" class="form-control" id="status" name="status">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-md btn-primary">Submit</button>
                    <button type="reset" class="btn btn-md btn-danger">Cancel</button>
                </div>
            </form>
        </div>
        <!-- /.content -->
    </section>
    <!-- /.main-section -->
@endsection