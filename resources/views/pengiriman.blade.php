@extends('base')
@section('content')
    <!-- Main Section -->
    <section class="main-section">
        <!-- Add Your Content Inside -->
        <div class="content">
            <!-- Remove This Before You Start -->
            @if(Session::has('alert-success'))
                <div class="alert alert-success">
                    <strong>{{ \Illuminate\Support\Facades\Session::get('alert-success') }}</strong>
                </div>
            @endif
            <hr>
            <a class="btn btn-success" href="{{ route('pengiriman.create') }}">Update Status Pengiriman</a>
            <hr>

            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>No.</th>
                    <th>No Pengiriman</th>
                    <th>Tanggal</th>
                    <th>Lokasi ID</th>
                    <th>Barang ID</th>
                    <th>Jumlah Barang</th>
                    <th>Harga Barang</th>
                    <th>Kurir ID</th>
                    <th>Status</th>
                    <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                @php $no = 1; @endphp
                @foreach($data as $datas)
                    <tr>
                        <td>{{ $no++ }}</td>
                        <td>{{ $datas->no_pengiriman }}</td>
                        <td>{{ $datas->tanggal }}</td>
                        <td>{{ $datas->lokasi_id }}</td>
                        <td>{{ $datas->barang_id }}</td>
                        <td>{{ $datas->jumlah_barang }}</td>
                        <td>{{ $datas->harga_barang }}</td>
                        <td>{{ $datas->kurir_id }}</td>
                        <td>{{ $datas->status }}</td>
                        <td>
                            <form action="{{ route('barang.destroy', $datas->id) }}" method="post">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <a href="{{ route('pengiriman.edit',$datas->id) }}" class=" btn btn-sm btn-primary">Edit</a>
                                <button class="btn btn-sm btn-danger" type="submit" onclick="return confirm('Yakin ingin menghapus data?')">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.content -->
    </section>
    <!-- /.main-section -->
@endsection