<?php

namespace App\Http\Controllers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\ModelPengiriman;

class PengirimanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = ModelPengiriman::all();
        return view('pengiriman',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function create()
    {
        return view('pengiriman_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new ModelPengiriman();
        $data->no_pengiriman = $request->no_pengiriman;
        $data->tanggal = $request->tanggal;
        $data->lokasi_id = $request->lokasi_id;
        $data->barang_id = $request->barang_id;
        $data->jumlah_barang = $request->jumlah_barang;
        $data->harga_barang = $request->harga_barang;
        $data->kurir_id = $request->kurir_id;
        $data->status = $request->status;
        $data->save();
        return redirect()->route('pengiriman.index')->with('alert-success','Data berhasil ditambah!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = ModelPengiriman::where('id',$id)->get();

        return view('pengiriman_edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = ModelPengiriman::where('id',$id)->first();
        $data->no_pengiriman = $request->no_pengiriman;
        $data->tanggal = $request->tanggal;
        $data->lokasi_id = $request->lokasi_id;
        $data->barang_id = $request->barang_id;
        $data->jumlah_barang = $request->jumlah_barang;
        $data->harga_barang = $request->harga_barang;
        $data->kurir_id = $request->kurir_id;
        $data->status = $request->status;
        $data->save();
        return redirect()->route('pengiriman.index')->with('alert-success','Data berhasil diubah!');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = ModelPengiriman::where('id',$id)->first();
        $data->delete();
        return redirect()->route('pengiriman.index')->with('alert-success','Data berhasi dihapus!');
    }
}
